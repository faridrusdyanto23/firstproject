import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import cart from '../../assets/icons/shopping-cart.png'

const Cart = (props) => {
  return (
    <View>
      <View style={styles.shopWrapper}>
        <Image source={cart} style={styles.iconShop} />
        <Text style={styles.notif}>{props.quantity}</Text>
      </View>
      <Text style={styles.text}>Keranjang Belanja Anda</Text>
    </View>
  )
}

export default Cart

const styles = StyleSheet.create({
  shopWrapper: {
    borderWidth: 1, 
    borderColor: '#4398D1', 
    width: 93, 
    height: 93, 
    borderRadius: 93/2,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    marginTop: 40
  },
  iconShop: {width: 50, height: 50},
  wrapper: {padding: 20, alignItems: 'center'},
  text: {fontSize: 12, color: '#777777', fontWeight: '700', marginTop: 8},
  notif: {
    fontSize: 12, 
    color: 'white', 
    backgroundColor: '#6fcf97', 
    padding: 4, 
    borderRadius: 25, 
    width: 24, 
    height: 24,
    position: 'absolute',
    top: 0,
    right: 0
  }
})
