/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import { ScrollView, View } from 'react-native';
import SampleComp from './pages/SampleComp/';
import StylingReactNativeComp from './pages/StylingComp';
import FlexBox from './pages/Flaxbox';
import Position from './pages/Position';
import PropsDinamis from './pages/PropsDinamis';
import StateDinamis from './pages/StateDinamis';
import Communication from './pages/Communication';
import Product from './components/Product';
import ReactNativeSvg from './pages/ReactNativeSvg';
import CallApiVanilla from './pages/CallApiVanilla';
import CallAPIAxios from './pages/CallAPIAxios';
import LocalAPI from './pages/LocalAPI';

const App = () => {
  const [isShow, setIsShow] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setIsShow(false);
    }, 6000);
  }, []);
  return (
    <View>
      <ScrollView>
        {/* <SampleComp/> */}
        {/* <StylingReactNativeComp/> */}
        {/* <FlexBox/> */}
        {/* <Position /> */}
        {/* <PropsDinamis /> */}
        {/* <StateDinamis/> */}
        {/* <Communication/> */}
        {/* <ReactNativeSvg/> */}
        {/* <CallApiVanilla/> */}
        {/* <CallAPIAxios/> */}
        <LocalAPI/>
      </ScrollView>
    </View>
  );
};

export default App;

