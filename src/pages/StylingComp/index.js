import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import macbook from '../../assets/img/macbook-pro2.jpg';

const StylingComp = () => {
    return (
      <View>
        <Text style={styles.text}>Styling React Native Component</Text>
        <View 
          style={{
            width: 100, 
            height: 100, 
            backgroundColor: 'blue', 
            borderWidth: 2,
            borderColor: 'red',
            marginTop: 20,
            marginLeft: 40,
            marginBottom: 50
          }}
        />
        <View 
          style={{
            padding: 12, 
            backgroundColor: '#F2F2F2', 
            width: 212, 
            borderRadius: 8,
            marginLeft: 20
          }}>
          <Image 
            source={macbook} 
            style={{
              width: 188, 
              height: 107, 
              borderRadius: 8
            }}
          />
          <Text 
            style={{
              fontSize: 14, 
              fontWeight: 'bold', 
              marginTop: 16
            }}>
              New Macbook Pro 2020
          </Text>
          <Text style={{fontSize: 12, fontWeight: 'bold', color: '#f2994a', marginTop: 12}}>
            Rp. 25.000.000
          </Text>
          <Text style={{fontSize: 12, fontWeight: '300', marginTop: 12,}}>
            Jakarta Barat
          </Text>
          <View style={{backgroundColor: '#6fcf97', paddingVertical: 6, borderRadius: 25, marginTop: 20}}>
            <Text style={{fontSize: 14, fontWeight: '600', color: 'white', textAlign: 'center'}}>
              BELI
            </Text>
          </View>
        </View>
      </View>
    )
  }
  
  const styles = StyleSheet.create({
    text: {
      fontSize: 18,
      fontWeight: 'bold',
      color: 'green',
      marginLeft: 70,
      marginTop: 40
    }
  })

  export default StylingComp;