import React, { Component } from 'react';
import { Image, Text, TextInput, View } from 'react-native';

const SampleComp = () => {
  return (
    <View>
      <View style={{width: 80, height: 80, backgroundColor: '#00a8ff'}} />
      <Text>Farid</Text>  
      <Text>Rusdyanto</Text>
      <Rusdy/>
      <Foto />
      <TextInput style={{borderWidth: 1}}/>
      <BoxGreen />
      <Profile />
    </View>
  )
}

const Rusdy = () => {
  return (
    <Text>tes Audit</Text>
  );
};

const Foto = () => {
  return (
    <Image 
      source={{uri: 'https://placeimg.com/100/100/animals'}}
      style={{width: 100, height: 100}}
    />
  );
};

class BoxGreen extends Component {
  render() {
    return (
      <Text>Ini komponen dari class komponen</Text>
    )
  }
}

class Profile extends Component {
  render() {
    return (
      <View>
        <Image 
          source={{uri: 'https://placeimg.com/100/100/arch'}} 
          style={{width: 100, height: 100, borderRadius: 50}} 
        />
        <Text style={{color: 'blue', fontSize: 24}}>Hewan</Text>
      </View>
    )
  }
}

export default SampleComp;