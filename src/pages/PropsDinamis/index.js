import React from 'react'
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native'

const Story = (props) => {

  return (
    <View style={{alignItems: 'center', marginRight: 20}}>
      <Image 
        source={{uri: props.gambar}}
        style={{width: 70, height: 70, borderRadius: 70 / 2}}
      />
      <Text style={{maxWidth: 50, textAlign: 'center'}}>{props.judul}</Text>
    </View>
  )
}

export default function PropsDinamis() {
  return (
    <View>
      <Text>Materi Component Dinamis dengan Props</Text>
      <ScrollView horizontal>
        <View style={{flexDirection: 'row'}}>
          <Story 
            judul="Youtube Componen" 
            gambar="https://placeimg.com/100/100/arch"
          />
          <Story 
            judul="Kelas Online" 
            gambar="https://placeimg.com/100/100/people"
          />
          <Story 
            judul="Tes" 
            gambar="https://placeimg.com/100/100/animals"
          />
          <Story 
            judul="oding" 
            gambar="https://placeimg.com/100/100/animals"
          />
          <Story 
            judul="yan koding" 
            gambar="https://placeimg.com/100/100/animals"
          />
          <Story 
            judul="Kabayan" 
            gambar="https://placeimg.com/100/100/animals"
          />
          <Story 
            judul="ding" 
            gambar="https://placeimg.com/100/100/animals"
          />
        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({})
