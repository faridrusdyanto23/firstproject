import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import IlustrationMyApp from '../../assets/img/undraw_app_data_re_vg5c.svg'

const ReactNativeSvg = () => {
  return (
    <View>
      <Text>Materi Menggunkan file SVG</Text>
      <IlustrationMyApp width={300} height={150}/>
    </View>
  )
}

export default ReactNativeSvg

const styles = StyleSheet.create({})
