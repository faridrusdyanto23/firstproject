import React, { useEffect, useState } from 'react';
// import { Component } from "react";
import { Image, Text, View } from 'react-native';

// class FlexBox extends Component {
//   constructor(props) {
//     super(props);
//     console.log('===> constructor');
//     this.state = {
//       subscriber: 300
//     }
//   }
//   componentDidUpdate() {
//     console.log('==> component didUpdate')
//   }

//   componentDidMount() {
//     console.log('===> didMount')
//     setTimeout(() => {
//       this.setState({
//         subscriber: 400
//       });
//     }, 2000)
//   }

//   componentWillUnmount() {
//     console.log('===> component wil unMount')
//   }
//   render() {
//     console.log('===> render')
//     return (
//       <View>
//         <View style={{
//           flexDirection: 'row', 
//           backgroundColor: '#ED4C67', 
//           alignItems: 'center',
//           justifyContent: 'space-between'
//         }}>
//           <View style={{backgroundColor: '#A3CB38', width: 50, height: 50}}></View>
//           <View style={{backgroundColor: '#0652DD', width: 50, height: 50}}></View>
//           <View style={{backgroundColor: '#73c1d5', width: 50, height: 50}}></View>
//           <View style={{backgroundColor: '#FDA7DF', width: 50, height: 50}}></View>
//         </View>
//         <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
//           <Text>Beranda</Text>
//           <Text>Video</Text>
//           <Text>PlayList</Text>
//           <Text>Komunitas</Text>
//           <Text>Chanel</Text>
//           <Text>Tentang</Text>
//         </View>
//         <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
//           <Image source={{
//             uri: 'https://placeimg.com/100/100/people'}} 
//             style={{width: 100, height: 100, borderRadius: 50, marginRight: 14}}
//           />
//           <View>
//             <Text style={{fontSize: 20, fontWeight: 'bold'}}>Var Tizero</Text>
//             <Text>{this.state.subscriber} ribu Subscriber</Text>
//           </View>
//         </View>
//       </View>
//     )
//   }
// }

const FlexBox = () => {
  const [subscriber, setSubscriber] = useState(200);

  useEffect(() => {
    console.log('did mount');
    setTimeout(() => {
      setSubscriber(500);
    }, 4000)
    return () => {
      console.log('did update')
    }
  }, [subscriber]);

  // useEffect(() => {
  //   console.log('did update')
  //   setTimeout(() => {
  //     setSubscriber(500);
  //   }, 4000)
  // }, [subscriber]);
  return (
    <View>
        <View style={{
          flexDirection: 'row', 
          backgroundColor: '#ED4C67', 
          alignItems: 'center',
          justifyContent: 'space-between'
        }}>
          <View style={{backgroundColor: '#A3CB38', width: 50, height: 50}}></View>
          <View style={{backgroundColor: '#0652DD', width: 50, height: 50}}></View>
          <View style={{backgroundColor: '#73c1d5', width: 50, height: 50}}></View>
          <View style={{backgroundColor: '#FDA7DF', width: 50, height: 50}}></View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text>Beranda</Text>
          <Text>Video</Text>
          <Text>PlayList</Text>
          <Text>Komunitas</Text>
          <Text>Chanel</Text>
          <Text>Tentang</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
          <Image source={{
            uri: 'https://placeimg.com/100/100/people'}} 
            style={{width: 100, height: 100, borderRadius: 50, marginRight: 14}}
          />
          <View>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>Var Tizero</Text>
            <Text>{subscriber} ribu Subscriber</Text>
          </View>
        </View>
      </View>
  )
}

export default FlexBox;